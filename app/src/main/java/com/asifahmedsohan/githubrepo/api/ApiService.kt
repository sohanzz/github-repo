package com.asifahmedsohan.githubrepo.api

import com.asifahmedsohan.githubrepo.data.model.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {

    @GET("search/repositories")
    suspend fun searchRepos(
        @Query("q") searchQuery: String,
        @Query("sort") sortQuery: String,
        @Query("page") pageIndex: Int,
        @Query("per_page") perPage: Int,
    ): Response<SearchResponse>

    @GET("search/repositories")
    suspend fun searchReposV2(
        @Query("q") searchQuery: String,
        @Query("page") pageIndex: Int,
        @Query("per_page") perPage: Int,
    ): Response<SearchResponse>

}