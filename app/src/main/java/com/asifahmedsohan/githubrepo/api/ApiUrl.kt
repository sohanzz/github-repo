package com.asifahmedsohan.githubrepo.api


class ApiUrl {
    companion object {
        const val STAGE_API_URL = "https://api.github.com/"
        const val PRODUCTION_API_URL = "https://api.github.com/"

        // Base URL
        private const val API_URL = STAGE_API_URL

        fun getBaseUrl(): String {
            return API_URL
        }
    }
}