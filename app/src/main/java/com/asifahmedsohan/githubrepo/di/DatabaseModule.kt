package com.asifahmedsohan.githubrepo.di

import android.content.Context
import androidx.room.Room
import com.asifahmedsohan.githubrepo.Utils.AppConstants
import com.asifahmedsohan.githubrepo.data.RepositoryDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ) : RepositoryDatabase {
        return Room.databaseBuilder(
            context,
            RepositoryDatabase::class.java,
            AppConstants.DATABASE_NAME
        ).build()
    }
}