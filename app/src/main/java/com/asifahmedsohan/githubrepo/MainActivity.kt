package com.asifahmedsohan.githubrepo

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.asifahmedsohan.githubrepo.Utils.AppUtils
import com.asifahmedsohan.githubrepo.Utils.SortEnums
import com.asifahmedsohan.githubrepo.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityMainBinding
    private lateinit var navController: NavController

    val sharedPreferences by lazy {
        getPreferences(Context.MODE_PRIVATE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        saveSharedPref()


        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_main) as NavHostFragment
        navController = navHostFragment.findNavController()
    }

    private fun saveSharedPref() {
        val sortType = sharedPreferences.getString(getString(R.string.sort_by), "")
        if (!AppUtils.isValid(sortType)) {
            sharedPreferences.edit().putString(getString(R.string.sort_by), SortEnums.default.name)
                .apply()
        }
    }
}