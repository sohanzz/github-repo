package com.asifahmedsohan.githubrepo.data.paging

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.asifahmedsohan.githubrepo.api.ApiService
import com.asifahmedsohan.githubrepo.data.RepositoryDatabase
import com.asifahmedsohan.githubrepo.data.model.RemoteKeys
import com.asifahmedsohan.githubrepo.data.model.Repository


@ExperimentalPagingApi
class RepositoryRemoteMediator(
    private val apiService: ApiService,
    private val repositoryDatabase: RepositoryDatabase,
    private val query: String,
    private val sortOrder: String,
    private val initialPage: Int = 1,
) : RemoteMediator<Int, Repository>() {

    private val repositoryDao = repositoryDatabase.repositoryDao()
    private val remoteKeysDao = repositoryDatabase.remoteKeysDao()

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, Repository>,
    ): MediatorResult {
        try {
            val page = when (loadType) {

                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    remoteKeys?.nextKey?.minus(1) ?: initialPage
                }

                LoadType.PREPEND -> {
                    val remoteKeys = getRemoteKeyForFirstItem(state)
                    val prevKey = remoteKeys?.prevKey
                        ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                    prevKey
                }

                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                    val nextKey = remoteKeys?.nextKey
                        ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                    nextKey
                }
            }

            val repos = apiService.searchRepos(searchQuery = query,
                pageIndex = page,
                perPage = 10,
                sortQuery = sortOrder)
            val endOfPaginationReached =
                if (repos.isSuccessful) repos.body()!!.items.isEmpty() else true

            val prevPage = if (page == 1) null else page - 1
            val nextPage = if (endOfPaginationReached) null else page + 1

            repositoryDatabase.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    repositoryDao.deleteAllRepositories()
                    remoteKeysDao.clearRemoteKeys()
                }
                val keys = repos.body()!!.items.map { repo ->
                    RemoteKeys(
                        repositoryId = repo.id,
                        prevKey = prevPage,
                        nextKey = nextPage
                    )
                }
                remoteKeysDao.insertAll(remoteKey = keys)
                repositoryDao.insertAllRepositories(repos = repos.body()!!.items)
            }

            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (e: Exception) {
            Log.e("Error", "load: $e")
            return MediatorResult.Error(e)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, Repository>): RemoteKeys? {
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { repo ->
                repositoryDatabase.remoteKeysDao().remoteKeysId(repo.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, Repository>): RemoteKeys? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { photo ->
                repositoryDatabase.remoteKeysDao().remoteKeysId(photo.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, Repository>,
    ): RemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                repositoryDatabase.remoteKeysDao().remoteKeysId(repoId)
            }
        }
    }
}