package com.asifahmedsohan.githubrepo.data.HomeRepository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.asifahmedsohan.githubrepo.Utils.SortEnums
import com.asifahmedsohan.githubrepo.Utils.Status
import com.asifahmedsohan.githubrepo.api.ApiService
import com.asifahmedsohan.githubrepo.data.RepositoryDatabase
import com.asifahmedsohan.githubrepo.data.model.Repository
import com.asifahmedsohan.githubrepo.data.model.SearchResponse
import com.asifahmedsohan.githubrepo.data.paging.RepositoryRemoteMediator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import retrofit2.Response
import javax.inject.Inject

@ExperimentalPagingApi
class GithubRepository @Inject constructor(
    private val apiService: ApiService,
    private val repositoryDatabase: RepositoryDatabase,
) {


    // unfortunately could not find any support to load data using sort query with pagination3
    fun getAllRepositories(query: String, sortOrder: String): Flow<PagingData<Repository>> {
        var pagingSourceFactory = { repositoryDatabase.repositoryDao().getAllRepositories() }

        return Pager(
            config = PagingConfig(pageSize = 10),
            remoteMediator = RepositoryRemoteMediator(
                apiService = apiService,
                repositoryDatabase = repositoryDatabase,
                query = query,
                sortOrder = sortOrder
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    suspend fun clearDatabase() {
        repositoryDatabase.repositoryDao().deleteAllRepositories()
    }

    fun getRepositoriesSortByStars(): Flow<Status<List<Repository>>> = flow {
        emitAll(repositoryDatabase.repositoryDao().getAllRepositoriesSortByStartsFlow().map {
            Status.Success(it)
        })
    }.flowOn(Dispatchers.IO)

    // TODO: store timestamp in model using the string format date in the api
    fun getRepositoriesSortByTime(): Flow<Status<List<Repository>>> = flow {
        emitAll(repositoryDatabase.repositoryDao().getAllRepositoriesSortByTimeFlow().map {
            Status.Success(it)
        })
    }.flowOn(Dispatchers.IO)

    fun getRepositoriesSortByDefault(): Flow<Status<List<Repository>>> = flow {
        emitAll(repositoryDatabase.repositoryDao().getAllRepositoriesSortByDefaultFlow().map {
            Status.Success(it)
        })
    }.flowOn(Dispatchers.IO)


    fun fetchRepositoriesRemote(
        searchQuery: String,
        networkStatus: Boolean,
        pageIndex: Int,
        sortType: String,
    ): Flow<Status<List<Repository>>> {

        return object : BaseRepository() {

            override fun networkAvailable(): Boolean = networkStatus

            override suspend fun fetchRemoteApi(): Response<SearchResponse> =
                apiService.searchReposV2(searchQuery, pageIndex, 10)

            override fun extractDataFromResponse(response: Response<SearchResponse>): List<Repository> =
                response.body()!!.items

            override suspend fun saveDataToRoom(data: List<Repository>) {
                repositoryDatabase.repositoryDao().insertAllRepositories(data)
            }

            override fun fetchLocal(): Flow<List<Repository>> {
                return when (sortType) {
                    SortEnums.stars.name -> {
                        repositoryDatabase.repositoryDao().getAllRepositoriesSortByStartsFlow()
                    }
                    SortEnums.updated.name -> {
                        repositoryDatabase.repositoryDao().getAllRepositoriesSortByTimeFlow()
                    }
                    else -> repositoryDatabase.repositoryDao().getRepositoriesFlow()
                }
            }

        }.asFlow().flowOn(Dispatchers.IO)

    }
}