package com.asifahmedsohan.githubrepo.data.HomeRepository

import com.asifahmedsohan.githubrepo.Utils.Status
import com.asifahmedsohan.githubrepo.data.model.Repository
import com.asifahmedsohan.githubrepo.data.model.SearchResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import retrofit2.Response


abstract class BaseRepository {

    fun asFlow() = flow {
        emit(Status.Loading())
        if (networkAvailable()) {
            try {
                val response = fetchRemoteApi()
                if (response.isSuccessful && response.body()!!.items.isNotEmpty()) {
                    val data = extractDataFromResponse(response)
                    saveDataToRoom(data)
                } else {
                    emit(Status.error(response.message()))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emit(Status.error("Error"))
            }
        }
        emitAll(fetchLocal().map {
            Status.success(it)
        })
    }


    protected abstract fun networkAvailable(): Boolean

    protected abstract suspend fun fetchRemoteApi(): Response<SearchResponse>

    protected abstract fun extractDataFromResponse(response: Response<SearchResponse>): List<Repository>

    protected abstract suspend fun saveDataToRoom(data: List<Repository>)

    protected abstract fun fetchLocal(): Flow<List<Repository>>

}