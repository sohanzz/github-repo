package com.asifahmedsohan.githubrepo.data.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.asifahmedsohan.githubrepo.Utils.AppConstants
import java.io.Serializable

@Entity(tableName = AppConstants.REPOSITORY_TABLE_NAME)
data class Repository(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val node_id: String,
    val name: String,
    val full_name: String,
    @Embedded
    val owner: Owner,
    val description: String?,
    val updated_at: String,
    val stargazers_count: Int,
    val watchers_count: Int,

    ) : Serializable {

}
