package com.asifahmedsohan.githubrepo.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "remote_keys")
class RemoteKeys(
    @PrimaryKey(autoGenerate = false)
    val repositoryId: String,
    val prevKey: Int?,
    val nextKey: Int?,
) {
}