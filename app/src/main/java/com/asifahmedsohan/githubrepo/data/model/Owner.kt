package com.asifahmedsohan.githubrepo.data.model

import kotlinx.serialization.Serializable

@Serializable
data class Owner(
    val login: String,
    val avatar_url: String,
) {

}
