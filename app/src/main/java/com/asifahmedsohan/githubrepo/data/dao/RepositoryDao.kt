package com.asifahmedsohan.githubrepo.data.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.asifahmedsohan.githubrepo.data.model.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@Dao
interface RepositoryDao {

    @Query("SELECT * FROM repository")
    fun getAllRepositories(): PagingSource<Int, Repository>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllRepositories(repos: List<Repository>)

    @Query("DELETE FROM repository")
    suspend fun deleteAllRepositories()


    @Query("SELECT * FROM repository")
    fun _getAllRepositoriesList(): List<Repository>

    fun getRepositoriesFlow(): Flow<List<Repository>> {
        val repos = _getAllRepositoriesList()
        return flow { emit(repos) }
    }

    @Query("SELECT * FROM repository")
    fun _getAllRepositoriesSortByDefaultFlow(): List<Repository>

    fun getAllRepositoriesSortByDefaultFlow(): Flow<List<Repository>> {
        val repos = _getAllRepositoriesSortByDefaultFlow()
        return flow { emit(repos) }
    }

    @Query("SELECT * FROM repository ORDER BY stargazers_count DESC")
    fun _getAllRepositoriesSortByStarts(): List<Repository>

    fun getAllRepositoriesSortByStartsFlow(): Flow<List<Repository>> {
        val repos = _getAllRepositoriesSortByStarts()
        return flow { emit(repos) }
    }

    @Query("SELECT * FROM repository ORDER BY updated_at DESC")
    fun _getAllRepositoriesSortByTime(): List<Repository>

    fun getAllRepositoriesSortByTimeFlow(): Flow<List<Repository>> {
        val repos = _getAllRepositoriesSortByTime()
        return flow { emit(repos) }
    }

}