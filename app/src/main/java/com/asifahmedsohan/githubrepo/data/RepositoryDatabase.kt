package com.asifahmedsohan.githubrepo.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.asifahmedsohan.githubrepo.data.dao.RemoteKeysDao
import com.asifahmedsohan.githubrepo.data.dao.RepositoryDao
import com.asifahmedsohan.githubrepo.data.model.RemoteKeys
import com.asifahmedsohan.githubrepo.data.model.Repository

@Database(
    entities = [Repository::class, RemoteKeys::class],
    version = 1
)
abstract class RepositoryDatabase : RoomDatabase() {

    abstract fun repositoryDao(): RepositoryDao
    abstract fun remoteKeysDao(): RemoteKeysDao
}