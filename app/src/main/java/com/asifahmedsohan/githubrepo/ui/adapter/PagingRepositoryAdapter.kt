package com.asifahmedsohan.githubrepo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.asifahmedsohan.githubrepo.R
import com.asifahmedsohan.githubrepo.data.model.Repository
import com.asifahmedsohan.githubrepo.databinding.ItemRepositoryBinding
import com.bumptech.glide.Glide


class PagingRepositoryAdapter(private val listener: OnItemClickListener) :
    PagingDataAdapter<Repository, PagingRepositoryAdapter.RepositoryViewHolder>(
        REPOSITORY_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRepositoryBinding.inflate(inflater, parent, false)
        return RepositoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        val repo = getItem(position)

        if (repo != null) {
            holder.itemView.apply {
                holder.view.tvRepositoryName.text = repo.full_name
                holder.view.tvOwnerName.text = "@${repo.owner.login}"
                holder.view.tvStarsCount.text = repo.stargazers_count.toString()

                Glide.with(holder.itemView.context)
                    .load(repo.owner.avatar_url)
                    .placeholder(R.drawable.ic_placeholder_avatar)
                    .error(R.drawable.ic_placeholder_avatar)
                    .into(holder.view.ivRepositoryImage)
            }
        }
    }

    inner class RepositoryViewHolder(val view: ItemRepositoryBinding) :
        RecyclerView.ViewHolder(view.root) {
        init {
            view.clRootItem.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val repo = getItem(position)
                    if (repo != null) {
                        listener.onItemClick(repo)
                    }
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(repository: Repository)
    }

    companion object {
        private val REPOSITORY_COMPARATOR = object : DiffUtil.ItemCallback<Repository>() {
            override fun areItemsTheSame(oldItem: Repository, newItem: Repository) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Repository, newItem: Repository) =
                oldItem == newItem
        }
    }
}

