package com.asifahmedsohan.githubrepo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.asifahmedsohan.githubrepo.R
import com.asifahmedsohan.githubrepo.data.model.Repository
import com.asifahmedsohan.githubrepo.databinding.ItemRepositoryBinding
import com.bumptech.glide.Glide

class RepositoryAdapter(private val listener: RepositoryAdapter.OnItemClickListener) :
    RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): RepositoryAdapter.RepositoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRepositoryBinding.inflate(inflater, parent, false)
        return RepositoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RepositoryAdapter.RepositoryViewHolder, position: Int) {
        val repo = differ.currentList[position]

        if (repo != null) {
            holder.itemView.apply {
                holder.view.tvRepositoryName.text = repo.full_name
                holder.view.tvOwnerName.text = "@${repo.owner.login}"
                holder.view.tvStarsCount.text = repo.stargazers_count.toString()

                Glide.with(holder.itemView.context)
                    .load(repo.owner.avatar_url)
                    .placeholder(R.drawable.ic_placeholder_avatar)
                    .error(R.drawable.ic_placeholder_avatar)
                    .into(holder.view.ivRepositoryImage)
            }
        }
    }


    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class RepositoryViewHolder(val view: ItemRepositoryBinding) :
        RecyclerView.ViewHolder(view.root) {
        init {
            view.clRootItem.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val repo = differ.currentList[position]
                    if (repo != null) {
                        listener.onItemClick(repo)
                    }
                }
            }
        }
    }


    private val differCallback = object : DiffUtil.ItemCallback<Repository>() {
        override fun areItemsTheSame(oldItem: Repository, newItem: Repository): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Repository, newItem: Repository): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    interface OnItemClickListener {
        fun onItemClick(repository: Repository)
    }
}
