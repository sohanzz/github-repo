package com.asifahmedsohan.githubrepo.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.asifahmedsohan.githubrepo.R
import com.asifahmedsohan.githubrepo.Utils.AppUtils
import com.asifahmedsohan.githubrepo.Utils.SortEnums
import com.asifahmedsohan.githubrepo.Utils.Status
import com.asifahmedsohan.githubrepo.data.model.Repository
import com.asifahmedsohan.githubrepo.data.paging.PaginationScrollListener
import com.asifahmedsohan.githubrepo.databinding.FragmentHomeBinding
import com.asifahmedsohan.githubrepo.ui.adapter.PagingRepositoryAdapter
import com.asifahmedsohan.githubrepo.ui.adapter.RepositoryAdapter
import com.asifahmedsohan.githubrepo.ui.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
@ExperimentalPagingApi
class HomeFragment : Fragment(R.layout.fragment_home), RepositoryAdapter.OnItemClickListener {

    val TAG = "Home Fragment"

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val homeViewModel by viewModels<HomeViewModel>()
    private lateinit var pagingRepositoryAdapter: PagingRepositoryAdapter

    private lateinit var repositoryAdapter: RepositoryAdapter

    private var isConnected: Boolean = true
    private var pagingIndex: Int = 1
    private var sortType: String = SortEnums.default.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentHomeBinding.bind(view)
        checkNetworkConnection()
        initRecyclerView()
        addScrollListener()
        initData()
    }

    private fun checkNetworkConnection() {
        isConnected = AppUtils.isConnected(context)
        if (!isConnected) {
            Toast.makeText(context?.applicationContext,
                "No internet connection!",
                Toast.LENGTH_SHORT).show()
        }
    }

    private fun initData() {
        //implementation using paging3
        /*lifecycleScope.launch {
             homeViewModel.repos.collectLatest {
                 repositoryAdapter.submitData(it)
             }
         }*/

        //implementation using repository pattern
        sortType = activity?.getPreferences(Context.MODE_PRIVATE)
            ?.getString(getString(R.string.sort_by), SortEnums.default.name)!!

        homeViewModel.fetchRepositoriesDataRemote("android", isConnected, pagingIndex, sortType)

        homeViewModel.repositories.observe(viewLifecycleOwner, Observer { status ->
            when (status) {
                is Status.Loading -> showProgressBar()
                is Status.Success -> {
                    hideProgressBar()
                    if (status.data.isNotEmpty()) {
                        repositoryAdapter.differ.submitList(status.data)
                        pagingIndex++
                    }
                }

                is Status.Error -> {
                    hideProgressBar()
                }
            }
        })
    }

    var isLoading = false
    var isLastPage = false

    private fun showProgressBar() {
        binding.pbProgressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.pbProgressBar.visibility = View.INVISIBLE
    }

    private fun initRecyclerView() {
        /*binding.rvRepoList.apply {
            layoutManager = LinearLayoutManager(activity)
            repositoryAdapter = RepositoryAdapter(this@HomeFragment)
            setHasFixedSize(false)
            adapter = repositoryAdapter
        }*/

        binding.rvRepoList.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            repositoryAdapter = RepositoryAdapter(this@HomeFragment)
            setHasFixedSize(false)
            adapter = repositoryAdapter
        }

        binding.srlRefreshLayout.setOnRefreshListener {
            sortType = SortEnums.default.name
            pagingIndex = 1
            activity?.getPreferences(Context.MODE_PRIVATE)?.edit()
                ?.putString(getString(R.string.sort_by), sortType)?.apply()

            homeViewModel.refreshApiData("android",
                isConnected,
                pagingIndex,
                sortType)

            binding.srlRefreshLayout.isRefreshing = false
        }
    }

    // TODO: Need to improve scroll listener trigger
    private fun addScrollListener() {
        binding.rvRepoList.addOnScrollListener(object :
            PaginationScrollListener(binding.rvRepoList.layoutManager as LinearLayoutManager) {
            override fun loadMoreItems() {
                isLoading = true;
                pagingIndex++
                homeViewModel.fetchRepositoriesDataRemote("android",
                    isConnected,
                    pagingIndex,
                    sortType)
                isLoading = false
            }

            override fun isLastPage(): Boolean {
                return isLastPage; }

            override fun isLoading(): Boolean {
                return isLoading; }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when {
            item.itemId === R.id.sort_by_date -> {
                activity?.getPreferences(Context.MODE_PRIVATE)?.edit()
                    ?.putString(getString(R.string.sort_by), SortEnums.updated.name)?.apply()
                Toast.makeText(context?.applicationContext,
                    "Sort by: ${SortEnums.updated.name}",
                    Toast.LENGTH_SHORT).show()
                homeViewModel.getRepositoriesData(SortEnums.updated)
                binding.rvRepoList.scrollToPosition(0)

            }

            item.itemId === R.id.sort_by_stars -> {
                activity?.getPreferences(Context.MODE_PRIVATE)?.edit()
                    ?.putString(getString(R.string.sort_by), SortEnums.stars.name)?.apply()
                Toast.makeText(context?.applicationContext,
                    "Sort by: ${SortEnums.stars.name}",
                    Toast.LENGTH_SHORT).show()
                homeViewModel.getRepositoriesData(SortEnums.stars)
                binding.rvRepoList.scrollToPosition(0)

            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onItemClick(repository: Repository) {
        val action =
            HomeFragmentDirections.actionHomeFragmentToRepositoryDetailsFragment(repository)
        findNavController().navigate(action)
    }
}