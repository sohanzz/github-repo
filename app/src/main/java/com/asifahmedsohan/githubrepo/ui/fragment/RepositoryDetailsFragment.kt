package com.asifahmedsohan.githubrepo.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.asifahmedsohan.githubrepo.R
import com.asifahmedsohan.githubrepo.Utils.AppUtils
import com.asifahmedsohan.githubrepo.databinding.FragmentRepositoryDetailsBinding
import com.bumptech.glide.Glide


class RepositoryDetailsFragment : Fragment(R.layout.fragment_repository_details) {

    private var _binding: FragmentRepositoryDetailsBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<RepositoryDetailsFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentRepositoryDetailsBinding.bind(view)

        initData()
    }

    private fun initData() {
        binding.tvAuthorName.text = "@${args.repo.owner.login}"
        binding.tvRepositoryName.text = args.repo.full_name
        binding.tvDescription.text = args.repo.description
        binding.tvLastUpdated.text = AppUtils.dateFormatter(args.repo.updated_at)
        binding.tvStartCount.text = args.repo.stargazers_count.toString()
        binding.tvWatchCount.text = args.repo.watchers_count.toString()

        Glide.with(this)
            .load(args.repo.owner.avatar_url)
            .placeholder(R.drawable.ic_placeholder_avatar)
            .error(R.drawable.ic_placeholder_avatar)
            .into(binding.ivUserImage)

    }

}