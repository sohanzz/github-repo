package com.asifahmedsohan.githubrepo.ui.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.cachedIn
import com.asifahmedsohan.githubrepo.Utils.SortEnums
import com.asifahmedsohan.githubrepo.Utils.Status
import com.asifahmedsohan.githubrepo.data.HomeRepository.GithubRepository
import com.asifahmedsohan.githubrepo.data.model.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalPagingApi
@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: GithubRepository,
) : ViewModel() {

    //implementation using paging3
    var repos =
        repository.getAllRepositories("android", SortEnums.stars.name).cachedIn(viewModelScope)

    //implementation using repository pattern
    val repositories = MediatorLiveData<Status<List<Repository>>>()
    var sortType = SortEnums.default

    private var runSortedByStars: Flow<Status<List<Repository>>> = emptyFlow()
    private var runSortedByDate: Flow<Status<List<Repository>>> = emptyFlow()
    private var runSortedByDefault: Flow<Status<List<Repository>>> = emptyFlow()


    fun fetchRepositoriesDataRemote(
        searchQuery: String,
        networkStatus: Boolean,
        pageIndex: Int,
        sortType: String,
    ) {
        viewModelScope.launch {
            repository.fetchRepositoriesRemote(searchQuery, networkStatus, pageIndex, sortType)
                .collect {
                    repositories.value = it
                }
        }
    }

    fun getRepositoriesData(sortType: SortEnums) = when (sortType) {
        SortEnums.stars -> {
            runSortedByStars = repository.getRepositoriesSortByStars()
            viewModelScope.launch {
                runSortedByStars.collect {
                    repositories.value = it
                }
            }
        }

        SortEnums.updated -> {
            runSortedByDate = repository.getRepositoriesSortByTime()
            viewModelScope.launch {
                runSortedByDate.collect {
                    repositories.value = it
                }
            }
        }

        SortEnums.default -> {
            runSortedByDefault = repository.getRepositoriesSortByDefault()
            viewModelScope.launch {
                runSortedByDefault.collect {
                    repositories.value = it
                }
            }
        }

    }.also {
        this.sortType = sortType
    }

    fun refreshApiData(
        searchQuery: String,
        networkStatus: Boolean,
        pageIndex: Int,
        sortType: String,
    ) {

        viewModelScope.launch {
            repository.clearDatabase()
            repository.fetchRepositoriesRemote(searchQuery, networkStatus, pageIndex, sortType)
                .collect {
                    repositories.value = it
                }
        }
    }

}



