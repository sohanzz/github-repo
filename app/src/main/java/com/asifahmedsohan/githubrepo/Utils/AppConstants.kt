package com.asifahmedsohan.githubrepo.Utils


object AppConstants {

    const val DATABASE_NAME = "repository_db.db"
    const val REPOSITORY_TABLE_NAME = "repository"
}