package com.asifahmedsohan.githubrepo.Utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


object AppUtils {


    fun isValid(stringInput: String?): Boolean {
        return stringInput != null && stringInput.trim().isNotEmpty()
    }

    fun isConnected(context: Context?): Boolean {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }


    fun dateFormatter(date: String): String {

        try {
            val dateTime = ZonedDateTime.parse(date)
            val res = dateTime.withZoneSameInstant(ZoneId.of("BST"))
                .format(DateTimeFormatter.ofPattern("MM-dd-yy hh:mm:ss a"))

            return res
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }
}