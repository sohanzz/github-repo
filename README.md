##  Github Repo
A sample project to display list of repositories using github api.

# App Screenshots

<img src="screenshots/screenshot1.png" width="425"/> <img src="screenshots/screenshot2.png" width="425"/> 


# App Architechure
* Followed [MVVM](https://developer.android.com/jetpack/guide#recommended-app-arch) (Model View View-Model) architure.
![](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png)


## App Features

- Display list of repositoties using the github api with android query
- caching for offline access
- Pagination with to fetch data from api
- Refresh API data with top-bottom swipe in list screen
- Repository details view
- Different build types stage/release

## App components

I have tried to use latest android components to try out in this project. Also, This app uses different open source libraries:

- Retrofit for network calls
- Dagger Hilt for depedency injection
- Room database
- Glide image loading library
- Navigation Component
- Coroutines

## App Workflow Overview
The app initials fetch 10 items from the api using **android** keyword. Each time user  scroll the recycler view, the app fetech new items from the API. Data is stored in the Room Database using the repository model. The database serves as the single source of truth. Database can be access from the viewmodel using the repository. User can refresh the API using swipe down feature. This would clear the database table and fetch new data from the API.

User can navigate to repository details screen from the list. Also, user can sort the items in the list using the sort option from the app bar.

At first i have tried to use the new Paging3 as it supports fetcing inbuilt. But i could not find support to sort the with paging3. It would create duplicates if we use custom functions to sort the data so i decided to use custom pagination for the app. But you can find the paging3 implementations commnented inside the codebase.

#### Unfinished tasks
- Sort using the last update time.
     - Map string format date from the API into timestamp to store in the database. Use the timestamp to sort by time.
- Time constraints in API data refresh
	- Store the API response time in databsae. When fetching new items compare the current time with the last stored time to match the constraints.
	- Also, Work manager can be used to for periodic API calls


### Future Scope
- Add user input option for search query.
- Add save/remove option to show the saved items in a different screen
